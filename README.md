# Analtester

A silly gem for generating minitest files

## Installation

    $ gem install analtester

## Usage

    $ cd myproject
    $ analtest

That's it.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
